#ifndef __KEY_H
#define __KEY_H	 
#include "sys.h" 

//////////////////////////////////////////////////////////////////////////////////	 

//按键IO端口定义
#define KEY0 		PFin(9)   
#define KEY1 		PFin(8)		
#define KEY2 		PFin(7)		
#define KEY3 	    PFin(6)		


//按键值定义
#define KEY0_DATA	  1
#define KEY1_DATA	  2
#define KEY2_DATA	  3
#define KEY3_DATA     4
#define KEY0_PRES 	  1
#define KEY1_PRES	  2
#define KEY2_PRES	  3
#define WKUP_PRES     4

#define KEY0s 		GPIO_ReadInputDataBit(GPIOE,GPIO_Pin_8) //PE8
#define KEY1s 		GPIO_ReadInputDataBit(GPIOE,GPIO_Pin_10)	//PE10
#define KEY2s 		GPIO_ReadInputDataBit(GPIOE,GPIO_Pin_11) //PE11
#define WK_UPs 	    GPIO_ReadInputDataBit(GPIOE,GPIO_Pin_14)	//PE14

//变量声明
extern u8   keydown_data;    //按键按下后就返回的值
extern u8   keyup_data;      //按键抬起返回值
extern u16  key_time;
extern u8   key_tem; 

//函数声明
void KEY_Init(void);	      //IO初始化
void key_scan(u8 mode);  		//按键扫描函数	
u8 KEY_Scan(u8 mode);  
#endif

