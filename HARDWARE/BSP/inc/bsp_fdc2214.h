#ifndef __BSP_FDC2214_H
#define __BSP_FDC2214_H

#include "stm32f4xx.h"

#define FDC2214_ADDR 0x2A

/*FDC2214各个寄存器地址*/
#define DATA_CH0 0x00     				//数据寄存器
#define DATA_LSB_CH0 0x01
#define DATA_CH1 0x02
#define DATA_LSB_CH1 0x03
#define DATA_CH2 0x04
#define DATA_LSB_CH2 0x05
#define DATA_CH3 0x06
#define DATA_LSB_CH3 0x07
#define RCOUNT_CH0 0x08    //
#define RCOUNT_CH1 0x09
#define RCOUNT_CH2 0x0A
#define RCOUNT_CH3 0x0B
//#define OFFSET_CH0 0x0C  //FDC2114
//#define OFFSET_CH1 0x0D
//#define OFFSET_CH2 0x0E
//#define OFFSET_CH3 0x0F
#define SETTLECOUNT_CH0 0x10
#define SETTLECOUNT_CH1 0x11
#define SETTLECOUNT_CH2 0x12
#define SETTLECOUNT_CH3 0x13
#define CLOCK_DIVIDERS_C_CH0 0x14       //时钟分频
#define CLOCK_DIVIDERS_C_CH1 0x15
#define CLOCK_DIVIDERS_C_CH2 0x16
#define CLOCK_DIVIDERS_C_CH3 0x17
#define STATUS 0x18                     //状态寄存器
#define ERROR_CONFIG 0x19 				//错误报告设置
#define CONFIG 0x1A  
#define MUX_CONFIG 0x1B
#define RESET_DEV 0x1C
#define DRIVE_CURRENT_CH0 0x1E          //电流驱动
#define DRIVE_CURRENT_CH1 0x1F
#define DRIVE_CURRENT_CH2 0x20
#define DRIVE_CURRENT_CH3 0x21
#define MANUFACTURER_ID 0x7E      //读取值：0x5449
#define DEVICE_ID 0x7F            //读取值：0x3055

/*
		CONFIG寄存器内宏定义
*/
#define ACTIVE_CHAN_CH0 			0x0000
#define ACTIVE_CHAN_CH1 			0x4000
#define ACTIVE_CHAN_CH2				0x8000
#define ACTIVE_CHAN_CH3				0xC000

#define SLEEP_MODE_ENABLE			0x2000


void InitSingleFDC2214(void);
void InitMultiFDC2214(void);//双通道
unsigned int ReadRegfdc2214(unsigned char add);
void InitSingleFDC2214_test(void);//单通道 外部晶振
void InitMultiFDC2214_test(void);//ch0 ch1 外部晶振

void SetClockDividValue(u8 CLOCK_DIVIDERS_C_CHx_addr,u16 CHx_FREF_DIVIDER,u16 CHx_FIN_SEL);			//设置时钟分频
void SetInitWaitTime(u8 SETTLECOUNT_CHx_addr,u16 value);				//设置传感器的振荡稳定等待时间
void setConversionTime(u8 RCOUNT_CHx_addr,u16 data);																				//设置转换时间（用于测量传感器的参考时钟周期数）

#endif
