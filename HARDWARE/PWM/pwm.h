#ifndef _TIMER_H
#define _TIMER_H
#include "sys.h"
 	

void TIM14_PWM_Init(u32 auto_data,u32 fractional);
void TIM13_PWM_Init(u32 auto_data,u32 fractional);
void TIM2_PWM_Init(u32 auto_data,u32 fractional);
#endif
