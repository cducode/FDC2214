#include "motorCtrl.h"
#include "usart.h"
//#include "timer.h"
//#include "main.h"

//extern int ctrlState, moffset;

void motorAInit(){
	GPIO_InitTypeDef  GPIO_InitStructure;

    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOF | RCC_AHB1Periph_GPIOG, ENABLE);//使能GPIOF时钟

    //GPIOF9,F10初始化设置
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3; // AAIN1 AAIN2 ABIN1 ABIN2 
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;//普通输出模式
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;//推挽输出
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;//100MHz
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;//上拉
    GPIO_Init(GPIOF, &GPIO_InitStructure);//初始化
    
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;   //ASTBY
    GPIO_Init(GPIOG, &GPIO_InitStructure);//初始化
    //GPIO_ResetBits(GPIOF, GPIO_Pin_0 | GPIO_Pin_1);
    //GPIO_SetBits(GPIOF, GPIO_Pin_2);
    
}

void motorBInit(){
	GPIO_InitTypeDef  GPIO_InitStructure;

    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOF | RCC_AHB1Periph_GPIOG, ENABLE);//使能GPIOF时钟

    //GPIOF9,F10初始化设置
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15; // BAIN1 BAIN2 BBIN1 BBIN2 
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;//普通输出模式
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;//推挽输出
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;//100MHz
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;//上拉
    GPIO_Init(GPIOF, &GPIO_InitStructure);//初始化
    
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;   //BSTBY
    GPIO_Init(GPIOG, &GPIO_InitStructure);//初始化
    //GPIO_ResetBits(GPIOF, GPIO_Pin_0 | GPIO_Pin_1);
    //GPIO_SetBits(GPIOF, GPIO_Pin_2);
    
}

void motorInit(void){
    motorAInit();
    
    wait(0);
}

void clockwise(int speed){
    if(speed > 499) speed = 498;
    if(speed < 0) speed = 1;
    TIM_SetCompare1(TIM14, speed);
    AAIN1 = 1;
    AAIN2 = 0;
}

void anticlockwise(int speed){
    if(speed > 499) speed = 499;
    if(speed < 0) speed = 1;
    TIM_SetCompare1(TIM14, speed);
    AAIN1 = 0;
    AAIN2 = 1;
}

void kill(){
    AAIN1 = 0;
    AAIN2 = 0;
    ASTBY = 0;
}

void wait(int ms){
    
    ASTBY = 1;
    AAIN1 = 0;
    AAIN2 = 0;
    ABIN1 = 0;
    ABIN2 = 0;
    
//    BSTBY = 1;
//    BAIN1 = 1;
//    BAIN2 = 0;
//    BBIN1 = 1;
//    BBIN2 = 0;
    
    delay_ms(ms);
}


void start(int x){
    switch(x){
        case 0:
            BAIN1 = 1;
            break;
        case 90:
            AAIN1 = 1;
            break;
        case 180:
            BBIN1 = 1;
            break;
        case 270:
            ABIN1 = 1;
            break;
    }
}


void stop(int x){
    switch(x){
        case 0:
            BAIN1 = 0;
            break;
        case 90:
            AAIN1 = 0;
            break;
        case 180:
            BBIN1 = 0;
            break;
        case 270:
            ABIN1 = 0;
            break;
    }
}