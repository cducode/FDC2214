#ifndef __MOTOR_H
#define __MOTOR_H
#include "sys.h"
#include "delay.h"

#define AAIN1 PFout(0)
#define AAIN2 PFout(1)
#define ASTBY PGout(0)
#define ABIN1 PFout(2)
#define ABIN2 PFout(3)

#define BAIN1 PFout(12)
#define BAIN2 PFout(13)
#define BSTBY PGout(1)
#define BBIN1 PFout(14)
#define BBIN2 PFout(15)

void motorInit(void);
void clockwise(int);        //˳ʱ��
void anticlockwise(int);    //��ʱ��
void kill(void);
void wait(int);
int getEoffset(void);
int getMoffset(void);
void start(int);
void end(int);

#endif

