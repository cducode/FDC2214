/*
		石头为			0
		剪刀为 			1
		布为			2 
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "solve.h"
#include "filter.h"
#include "sys.h"
#include "delay.h"
#include "usart.h"
#include "oled.h"
#include "fdc2214.h"
#include "bsp.h"
#include "knnTest.h"
#include "knndata.h"


extern struct FDC fdcdata;

struct distanceData{
	float d;
	int type;
};

/*
	第0列为样本类型 
*/
void q_sort(int l ,int r,struct distanceData* nums);

float sample[1][4] = {0};
int hash[5] = {0,0,0,0,0};
struct distanceData res[700];           	//存放距离
struct distanceData result[TOPK];         	//TOPk的结果;    

int knn1(float train[][5],float sample[1][4],int x,int y,int k)
{
	int i , j ;
//	float *distance;
	float temp;
	int max = 0;
	
//	res = (struct distanceData*)malloc(sizeof(struct distanceData)*x);
//	result = (struct distanceData*)malloc(sizeof(struct distanceData)*k);
    
    for( i = 0 ; i < x ; i++)
        res[i].d = 0;
    
    for( i = 0 ; i < x ; i++){
        res[i].type = (int)train[i][0];
    }
    
    for( i = 0 ; i < 5 ; i++ )	//hash表初始化
        hash[i] = 0;
    
	for( i = 0 ; i < x ; i++ )
	{
		for( j = 1 ; j < y ; j++)
		{
			temp = abs( sample[0][j-1] - train[i][j] );	//与每个训练样本距离计算
			res[i].d += temp*temp;
		}
		res[i].d = sqrt(res[i].d);
	}
    
    q_sort(0,x,res);	//排序
    
	for( i = 0 ; i < k ; i++ )
	{
//        printf("%d  %d\r\n", i,res[i].type);
		hash[res[i].type]++;	// hash表操作，
	}
    
	for( i = 0 ; i < 5 ; i++)
	{
		if( max < hash[i] )
			max = i;
	}

    
//	free(res);
//	free(result);
    
	return max + 6;
}

int knn2(float train[][5],float sample[1][4],int x,int y,int k)
{
	int i , j ;
//	float *distance;
	float temp;
	int max = 0;
	
//	res = (struct distanceData*)malloc(sizeof(struct distanceData)*x);
//	result = (struct distanceData*)malloc(sizeof(struct distanceData)*k);
    
    for( i = 0 ; i < x ; i++)
        res[i].d = 0;
    
    for( i = 0 ; i < x ; i++){
        res[i].type = (int)train[i][0];
    }
    
    for( i = 0 ; i < 5 ; i++ )
        hash[i] = 0;
    
	for( i = 0 ; i < x ; i++ )
	{
		for( j = 1 ; j < y ; j++)
		{
			temp = abs( sample[0][j-1] - train[i][j] );
			res[i].d += temp*temp;
		}
		res[i].d = sqrt(res[i].d);
	}
//    printf("OK\r\n");
    q_sort(0,x,res);
    
//    printf("qsort_OK\r\n");
    
	for( i = 0 ; i < k ; i++ )
	{
//        printf("%d  %d\r\n", i,res[i].type);
		hash[res[i].type]++;
	}
    
//    printf("for_OK\r\n");
    
	for( i = 0 ; i < 5 ; i++)
	{
		if( max < hash[i] )
			max = i;
	}

    
//	free(res);
//	free(result);
    
	return max + 1;
}


int knn3(float train[][5],float sample[1][4],int x,int y,int k)
{
	int i , j ;
//	float *distance;
	float temp;
	int max = 0;
	
//	res = (struct distanceData*)malloc(sizeof(struct distanceData)*x);
//	result = (struct distanceData*)malloc(sizeof(struct distanceData)*k);
    
    for( i = 0 ; i < x ; i++)
        res[i].d = 0;
    
    for( i = 0 ; i < x ; i++){
        res[i].type = (int)train[i][0];
    }
    
    for( i = 0 ; i < 5 ; i++ )
        hash[i] = 0;
    
	for( i = 0 ; i < x ; i++ )
	{
		for( j = 1 ; j < y ; j++)
		{
			temp = abs( sample[0][j-1] - train[i][j] );
			res[i].d += temp*temp;
		}
		res[i].d = sqrt(res[i].d);
	}
//    printf("OK\r\n");
    q_sort(0,x,res);
    
//    printf("qsort_OK\r\n");
    
	for( i = 0 ; i < k ; i++ )
	{
//        printf("%d  %d\r\n", i,res[i].type);
		hash[res[i].type]++;
	}
    
//    printf("for_OK\r\n");
    
	for( i = 0 ; i < 5 ; i++)
	{
		if( max < hash[i] )
			max = i;
	}

    
//	free(res);
//	free(result);
    
	return max + 6;
}


int knn4(float train[][5],float sample[1][4],int x,int y,int k)
{
	int i , j ;
//	float *distance;
	float temp;
	int max = 0;
	
//	res = (struct distanceData*)malloc(sizeof(struct distanceData)*x);
//	result = (struct distanceData*)malloc(sizeof(struct distanceData)*k);
    
    for( i = 0 ; i < x ; i++)
        res[i].d = 0;
    
    for( i = 0 ; i < x ; i++){
        res[i].type = (int)train[i][0];
    }
    
    for( i = 0 ; i < 5 ; i++ )
        hash[i] = 0;
    
	for( i = 0 ; i < x ; i++ )
	{
		for( j = 1 ; j < y ; j++)
		{
			temp = abs( sample[0][j-1] - train[i][j] );
			res[i].d += temp*temp;
		}
		res[i].d = sqrt(res[i].d);
	}
//    printf("OK\r\n");
    q_sort(0,x,res);
    
//    printf("qsort_OK\r\n");
    
	for( i = 0 ; i < k ; i++ )
	{
//        printf("%d  %d\r\n", i,res[i].type);
		hash[res[i].type]++;
	}
    
//    printf("for_OK\r\n");
    
	for( i = 0 ; i < 5 ; i++)
	{
		if( max < hash[i] )
			max = i;
	}

    
//	free(res);
//	free(result);
    
	return max + 1;
}



void q_sort(int l ,int r , struct distanceData* nums)
{
	float key;
	struct distanceData temp;
	int i , j;
	if( l == r )
	return ;
	key = nums[l].d;
	i = l ;
	j = i+1;
	while( j < r )
	{
		if( key > nums[j].d )
		{
			temp = nums[i+1];
			nums[i+1] = nums[j];
			nums[j] = temp;
			i++;
			j++;
		}
		else
		j++;
	}
	temp = nums[l];
	nums[l] = nums[i];
	nums[i] = temp;
	q_sort(l,i,nums);
	q_sort(i+1,r,nums);
	return;
}


int get_shoushi(int data0, int data1, int data2, int data3, int type){
    sample[0][0] = data0;
    sample[0][1] = data1;
    sample[0][2] = data2;
    sample[0][3] = data3;
    
//    printf("%d %d %d %d\r\n", data0, data1, data2, data3);
    

	// knnx(训练样本数组， 测试样本数组， 训练样本数组大小， 训练样本数组元素长度， topk范围)；
    if(type == 1){
        return knn1(train1, sample, 599, 5, 5);     // 记得改数据大小
    }else if(type == 2){
        return knn2(train2, sample, 280, 5, 5);     // 记得改数据大小
    }else if(type == 3){
        return knn3(train3, sample, 45, 5, 5);     // 记得改数据大小
    }else if(type == 4){
        return knn4(train4, sample, 60, 5, 5);     // 记得改数据大小
    }
    
    return -1;
}

//int main()
//{
//	int res;
//	float sample[1][4] = {};
//	while(~scanf("%f%f%f%f",&sample[0][1],&sample[0][2],&sample[0][3],&sample[0][4]))
//	{
//		res = knn(train,sample,599,5,5);
//		printf("%d ",res);
//	}
//	return 0;
// } 

