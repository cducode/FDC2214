#include "state_judge.h"
#include "solve.h"
#include "filter.h"
#include "sys.h"
#include "delay.h"
#include "usart.h"
#include "oled.h"
#include "fdc2214.h"
#include "bsp.h"

#include "stdlib.h"
#include "string.h"
#include "math.h"


float data[3] = { 0 , 0 , 0 };		// data[0]  为当前数据
									// data[1] 	为前一次数据
									// data[2]	为前前次数据 
									
/*函数返回值
	1为发生跳变
	0为未发生跳变	
*/
int judgedata1,judgedata2;
int jumpFlag = 0;

int judgeState(float filterValue,float trueValue)
{
	float err1,err2;
	data[2] = data[1];
	data[1] = data[2];
	data[0] = trueValue;	//获取当前的值 
	
	err1 = data[2] - data[1];
	err2 = data[1] - data[0];
	
	if(abs(err1 - err2) > 5)
	{
		judgedata1 = data[1];
		jumpFlag = 1;
		return 1;
	}
	else
	{
		if(jumpFlag == 1)
		{
			judgedata2 = data[1];
			jumpFlag = 0;
        }
		return 0;
	}
}
