#include "solve.h"
#include "filter.h"
#include "sys.h"
#include "delay.h"
#include "usart.h"
#include "oled.h"
#include "fdc2214.h"
#include "bsp.h"
#include "state_judge.h"
#include "knnTest.h"
#include "debug.h"

#include "stdlib.h"
#include "string.h"
#include "math.h"


extern struct FDC fdcdata;
extern avefilterd data_filter_struct[4];   //结构体
extern float inDataAll[4];                 //传当前值，返回滤波后的值
extern struct s sdata;
extern float lastmean[4][5];               //4个通道前5次的平均值，递推窗口



struct FDC fdcdata;
struct FIL fdcfilter;           //
struct SFLAG stableFlag;        //稳定标志
struct ERR errdata;             //最大值最小值记录 



struct DOWN{
    int data0;
    int data1;
    int data2;
    int data3;
}downing = {0, 0, 0, 0};

//struct s{
//    u32 data0min;
//    u32 data0max;
//    u32 data1min;
//    u32 data1max;
//    u32 data2min;
//    u32 data2max;
//    u32 data3min;
//    u32 data3max;
//} sdata;

float errorDisplay(float data)
{
    static float last = 0;
    static float now = 0;
    static int count = 0;
    float error;
    if(count == 3)
    {
        now = data;
        error = abs(last - now);
        last = now;
        count = 0;
        return error;
    }
    else
    {
        count++;
        return -1;
    }
}

void get_fdcdata(int dex){
    int temp;

    switch(dex){
        case 0:
            temp = ReadRegfdc2214(0x00); 
            fdcdata.data0 = (temp << 16) | ReadRegfdc2214(0x01);
            fdcdata.cap0 = CAPDEN/(fdcdata.data0);
            fdcdata.cap0 *= fdcdata.cap0;
            break;
        case 1:
            temp = ReadRegfdc2214(0x02); 
            fdcdata.data1 = (temp << 16) | ReadRegfdc2214(0x03);
            fdcdata.cap1 = CAPDEN/(fdcdata.data1);
            fdcdata.cap1 *= fdcdata.cap1;
            break;
        case 2:
            temp = ReadRegfdc2214(0x04); 
            fdcdata.data2 = (temp << 16) | ReadRegfdc2214(0x05);
            fdcdata.cap2 = CAPDEN/(fdcdata.data2);
            fdcdata.cap2 *= fdcdata.cap2;
            break;
        case 3:
            temp = ReadRegfdc2214(0x06); 
            fdcdata.data3 = (temp << 16) | ReadRegfdc2214(0x07);
            fdcdata.cap3 = CAPDEN/(fdcdata.data3);
            fdcdata.cap3 *= fdcdata.cap3;
            break;
    }
}


void get_fdcall(){
    get_fdcdata(0);
    get_fdcdata(1);
    get_fdcdata(2);
    get_fdcdata(3);
}


void get_state(){        // 下降返回1， 平稳返回0；
//    static int res;

//    OLED_ShowChar(0, 0, (u8)downing.data0 + '0');

//    if(downing.data0 == 0 && fdcfilter.data0f - fdcdata.data0 > 20.000001){
//        errdata.data0max = fdcdata.data0;
//        errdata.data1max = fdcdata.data1;
//        errdata.data2max = fdcdata.data2;
//        errdata.data3max = fdcdata.data3;
//        downing.data0 = 1;
//    }
//    
//    if(downing.data0 == 1 && fdcfilter.data0f - fdcdata.data0 < 10.000001){
//        errdata.data0min = fdcdata.data0;
//        errdata.data1min = fdcdata.data1;
//        errdata.data2min = fdcdata.data2;
//        errdata.data3min = fdcdata.data3;
//        
////        res = get_shoushi(errdata.data0max - errdata.data0min, errdata.data1max - errdata.data1min, errdata.data2max - errdata.data2min, errdata.data3max - errdata.data3min);
//        printf("res:%d\r\n", res);
//        
//        
//        downing.data0 = 0;
//    }
   
}

void print_shoushi(int type){
    int shoushi;
//    printf("result:%d \r\n", get_shoushi(sdata.data0max - sdata.data0min, sdata.data1max - sdata.data1min, sdata.data2max - sdata.data2min, sdata.data3max - sdata.data3min));

    //传入每个通道的跳变差值
    // shoushi = get_shoushi(sdata.data0max - sdata.data0min, sdata.data1max - sdata.data1min, sdata.data2max - sdata.data2min, sdata.data3max - sdata.data3min, type);
//    shoushi = get_shoushi(lastmean[0][0] - fdcdata.data0, lastmean[1][0] - fdcdata.data1, lastmean[2][0] - fdcdata.data2, lastmean[3][0] - fdcdata.data3, type);

    //传入每个通道的跳变差值，lastmean存的是每个通道前五个窗口的平均值，窗口宽20，取lastmean[x][0]的值为放手钱的平稳值，lastmean[x][4]表示放手后的平稳值；
    shoushi = get_shoushi(lastmean[0][0] - lastmean[0][4], lastmean[1][0] - lastmean[1][4], lastmean[2][0] - lastmean[2][4], lastmean[3][0] - lastmean[3][4], type);
    
    res_show(shoushi);  // 屏幕显示识别结果
}
