#ifndef __SOLVE_H
#define __SOLVE_H
#include "sys.h"

#define CAPDEN 232021045.248

struct FDC{     //原始测量值 和 转换后的电容值
    int data0;
    float cap0;
    int data1;
    float cap1;
    int data2;
    float cap2;
    int data3;
    float cap3;
};

struct FIL{     //滤波后的值
    float data0f;
    float cap0f;
    float data1f;
    float cap1f;
    float data2f;
    float cap2f;
    float data3f;
    float cap3f;
};

struct SFLAG{       //稳定标志
    int data0stable;
    int data1stable;
    int data2stable;
    int data3stable;
};

struct ERR{         //
    float data0max;
    float data1max;
    float data2max;
    float data3max;
    float data0min;
    float data1min;
    float data2min;
    float data3min;
};

struct StateValueStruct{
    float data0;
    float data1;
    float data2;
    float data3;
};

//struct FDC fdcdata;

void get_fdcdata(int);
void get_fdcall(void);
void get_state(void);
void print_shoushi(int type);

float errorDisplay(float data);

#endif
