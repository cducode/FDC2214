#ifndef __FILTER_H
#define __FILTER_H
#include "sys.h"

#define LEN 5

typedef struct {
    u32 *buf;       //窗口地址
    int buflen;     //窗口长度
    int t;          //当前序号
    float sum;      //和
} avefilterd;

typedef struct {
    float *buf;       //窗口地址
    int buflen;     //窗口长度
    int t;          //当前序号
    float sum;      //和
} avefilterf;


float kalmanFilter_A(u32 inData);   //卡尔曼
float kalmanFilter_B(float inData);   //卡尔曼
float getAveFilterD(avefilterd *, u32);    //递推平均
float getAveFilterF(avefilterf *, float);
void ave_filter_init(void);
void kalmanFilter_ALL(void);

float get_stablevar(void);        //方差确定稳定性


#endif
