#include "filter.h"
#include "sys.h"
#include "delay.h"
#include "usart.h"
#include "lcd.h"
#include "solve.h"

#include "stdlib.h"
#include "string.h"
#include "math.h"

#define LEN 5

extern struct FDC fdcdata;

/***************卡尔曼*********************/
static float prevData=0; 
static float p=10, q=0.01, r=0.05, kGain=0;   //Q增大，动态响应变快，收敛性变坏

float kalmanFilter_A(u32 inData) 
{
    p = p+q; 
    kGain = p/(p+r);

    inData = prevData+(kGain*(inData-prevData)); 
    p = (1-kGain)*p;

    prevData = inData;

    return inData; 
}


static float prevDataB=0; 
static float pB=10, qB=0.0001, rB=0.005, kGainB=0;   //Q增大，动态响应变快，收敛性变坏

float kalmanFilter_B(float inDataB) 
{
    pB = pB+qB; 
    kGainB = pB/(pB+rB);

    inDataB = prevDataB +(kGainB*(inDataB-prevDataB)); 
    pB = (1-kGainB)*pB;

    prevDataB = inDataB;

    return inDataB; 
}


static float prevDataAll[4] = {0};
static float pAll[4]={10, 10, 10, 10}, qAll[4]={0.001, 0.001, 0.001, 0.001}, rAll[4]={0.05, 0.05, 0.05, 0.05}, kGainAll[4]={0, 0, 0, 0};   //Q增大，动态响应变快，收敛性变坏
float inDataAll[4] = {0};

void kalmanFilter_ALL() 
{
    int i = 0;
    for(; i < 4; i++){
        pAll[i] = pAll[i] + qAll[i];
        kGainAll[i] = pAll[i] / (pAll[i] + rAll[i]);
        
        inDataAll[i] = prevDataAll[i] + (kGainAll[i] * (inDataAll[i] - prevDataAll[i])); 
        pAll[i] = (1 - kGainAll[i]) * pAll[i];

        prevDataAll[i] = inDataAll[i];
    }
}



u32 data_filter_buf[4][LEN] = {0};
float cap_filter_buf[4][LEN] = {0};

avefilterd data_filter_struct[4];   //结构体


/*****************递推平均***************************/

void ave_filter_init(){     //
    int i = 0;

    for(; i < 4; i++){
        data_filter_struct[i].buf = &data_filter_buf[i][0];
        data_filter_struct[i].buflen = LEN;
        data_filter_struct[i].sum = 0;
        data_filter_struct[i].t = 0;
    }
}

float getAveFilterD(avefilterd *d, u32 data){
    d->sum = d->sum - (d->buf)[d->t] + data;
    (d->buf)[d->t] = data;
    d->t = d->t + 1;
    if(d->t == d->buflen) d->t = 0;
   
    return d->sum / d->buflen;
}

float getAveFilter(avefilterf *f, float data){
    f->sum = f->sum - (f->buf)[f->t] + data;
    (f->buf)[f->t] = data;
    f->t = f->t + 1;
    if(f->t == f->buflen) f->t = 0;
   
    return f->sum / f->buflen;
}



/*******************方差************************/

float lastmean[4][5] = {0.0}, bufmean[4];  // 上次5个窗口的平均值, 和方差
int varbuf[4][20] = {0}, varbuflen = 20, vart = 0, lastmeant = 0;
float threshold;    //阈值
float lastavr = 0;

float lastvar = 0;      //上一次var方差

float get_mean(int dex){
    int i = 0;
    int bufsum = 0;
    
    for(; i < varbuflen; i++){
        bufsum += varbuf[dex][i];
    }
    
    bufmean[dex] = bufsum / varbuflen;
    return bufmean[dex];
}

float get_var(int dex){
    int i = 0;
    float varsum = 0.0;
    
    for(; i < varbuflen; i++){
        varsum += pow(varbuf[dex][i] - bufmean[dex], 2);
    }
    
    return varsum / varbuflen;
}


/*
  返回当前窗口的方差，一个窗口表示二十个测量值，并记录前五个窗口的平均值
*/
float get_stablevar(){
    float mean, var, temp;
    int i = 0, j;
    
    varbuf[0][vart] = fdcdata.data0;
    varbuf[1][vart] = fdcdata.data1;
    varbuf[2][vart] = fdcdata.data2;
    varbuf[3][vart++] = fdcdata.data3;
    
    for(; i < 4; i++){
        if(vart == 20){
            mean = get_mean(i);
            if(i == 0){
                var = get_var(0);
                lastvar = var;
            }
            if(i == 3) vart = 0;

            if(lastmeant < 5){
                lastmean[i][lastmeant] = mean;
                if(i == 3) lastmeant++;
            }else{
                printf("B%d ", i);
                for(j = 0; j < lastmeant - 1; j++){
                    lastmean[i][j] = lastmean[i][j + 1];
                }
                lastmean[i][j] = mean;
            }
        }
    }
    
    
    
    
    return lastvar;
    
}


