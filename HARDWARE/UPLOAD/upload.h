#ifndef __UPLOAD_H
#define __UPLOAD_H
#include "sys.h"

void uploadD(u8 flag, u32 *, u32 *, u32 *, u32 *);
void uploadF(u8 flag, float *, float *, float *, float *);
void upload2(float *);
void upload3(int *);

void usartsendB(u8);
void usartsend(u8 *, u8);

#endif
