#include "upload.h"
#include "usart.h"
#include "sys.h"

u8 data1[21] = {0xAA, 0xAA};	//0xAAAA 0xF1 LEN DATA SUM
u8 sum1 = 0xAA + 0xAA;

u8 data2[17] = {0xAA, 0xAA, 0xF2};	//0xAAAA 0xF2 LEN DATA SUM
u8 sum2 = 0xAA + 0xAA + 0xF2;

u8 data3[17] = {0xAA, 0xAA, 0xF3};	//0xAAAA 0xF2 LEN DATA SUM
u8 sum3 = 0xAA + 0xAA + 0xF3;

void usartsendB(u8 data){
    while((USART3->SR&0X40)==0);//循环发送,直到发送完毕   
    USART3->DR = (u8)data;  
}

void usartsend(u8 *data, u8 len){
	int i = 0;	
	for(; i < len; i++){
		usartsendB((u8)data[i]);
	}
}

void uploadD(u8 flag, u32 *a, u32 *b, u32 *c, u32 *d){
	int i = 2, j;
	u8 *tmp = (u8 *)a;
    
    data1[i++] = flag;  //帧标志；
    data1[i++] = 4 * 4; // 数据个数 * 4
    
	data1[i++] = tmp[3];   
	data1[i++] = tmp[2];
	data1[i++] = tmp[1];
	data1[i++] = tmp[0];
    
	tmp = (u8 *)b;
    data1[i++] = tmp[3];   
	data1[i++] = tmp[2];
	data1[i++] = tmp[1];
	data1[i++] = tmp[0];
    
	tmp = (u8 *)c;
    data1[i++] = tmp[3];   
	data1[i++] = tmp[2];
	data1[i++] = tmp[1];
	data1[i++] = tmp[0];
    
	tmp = (u8 *)d;
    data1[i++] = tmp[3]; 
	data1[i++] = tmp[2];
	data1[i++] = tmp[1];
	data1[i++] = tmp[0];
    
	data1[i++] = 0;	//不校验
	// for(j = i - 1; j > 2; j--){	// 选择不校验
    //     data1[i] += (u8)data1[j];
    // }
    
	usartsend(data1, i);    //数据1
}

void uploadF(u8 flag, float *a, float *b, float *c, float *d){
	int i = 2, j;
	u8 *tmp = (u8 *)a;
    
    data1[i++] = flag;  //帧标志；
    data1[i++] = 4 * 4; // 数据个数 * 4
    
	data1[i++] = tmp[3];   
	data1[i++] = tmp[2];
	data1[i++] = tmp[1];
	data1[i++] = tmp[0];
    
	tmp = (u8 *)b;
    data1[i++] = tmp[3];   
	data1[i++] = tmp[2];
	data1[i++] = tmp[1];
	data1[i++] = tmp[0];
    
	tmp = (u8 *)c;
    data1[i++] = tmp[3];   
	data1[i++] = tmp[2];
	data1[i++] = tmp[1];
	data1[i++] = tmp[0];
    
	tmp = (u8 *)d;
    data1[i++] = tmp[3]; 
	data1[i++] = tmp[2];
	data1[i++] = tmp[1];
	data1[i++] = tmp[0];
    
	data1[i++] = 0;	//不校验
	// for(j = i - 1; j > 2; j--){	// 选择不校验
    //     data1[i] += (u8)data1[j];
    // }
    
	usartsend(data1, i);    //数据1
}


void upload2(float *capave){
    int i = 3, j;
	u8 *tmpd = (u8 *)capave;
	
    data2[i++] = 1 * 4; // 数据个数 * 4
    
	data2[i++] = tmpd[3];   //平均
	data2[i++] = tmpd[2];   
	data2[i++] = tmpd[1];
	data2[i++] = tmpd[0];
    
    for(j = i - 1; j > 3; j--){
        data2[i] += (u8)data2[j];
    }

    usartsend(data2, ++i);
}

void upload3(int *d){
    int i = 3, j;
	u8 *tmpd = (u8 *)d;
	
    data3[i++] = 1 * 4; // 数据个数 * 4
    
	data3[i++] = tmpd[3];   //平均
	data3[i++] = tmpd[2];   
	data3[i++] = tmpd[1];
	data3[i++] = tmpd[0];
    
    for(j = i - 1; j > 3; j--){
        data3[i] += (u8)data3[j];
    }

    usartsend(data3, ++i);
}

