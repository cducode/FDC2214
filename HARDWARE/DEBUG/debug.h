#ifndef __DEBUG_H
#define __DEBUG_H
#include "sys.h"

struct s{
    int data0min;
    int data0max;
    int data1min;
    int data1max;
    int data2min;
    int data2max;
    int data3min;
    int data3max;
};

void debug(void);
void getmin(void);
void getmax(void);
void autoUpload(void);
void print_shoushi(int type);

#endif

