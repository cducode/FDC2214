#include "debug.h"
#include "sys.h"
#include "delay.h"
#include "usart.h"
#include "led.h"
#include "beep.h"
#include "key.h"
#include "lcd.h"
#include "fdc2214.h"
#include "bsp.h"
#include "filter.h"
#include "oled.h"
#include "solve.h"
#include "upload.h"
#include "knnTest.h"

extern struct FDC fdcdata;

struct s sdata;


void getmin(){
    sdata.data0min = fdcdata.data0;
    sdata.data1min = fdcdata.data1;
    sdata.data2min = fdcdata.data2;
    sdata.data3min = fdcdata.data3;
}

void getmax(){
    sdata.data0max = fdcdata.data0;
    sdata.data1max = fdcdata.data1;
    sdata.data2max = fdcdata.data2;
    sdata.data3max = fdcdata.data3;
}

//void geterr(){
//    
//}

void autoUpload(){
    usartsendB(0xAA);
    usartsendB(0xAA);
    upload3(&sdata.data0min);
    upload3(&sdata.data0max);
    upload3(&sdata.data1min);
    upload3(&sdata.data1max);
    upload3(&sdata.data2min);
    upload3(&sdata.data2max);
    upload3(&sdata.data3min);
    upload3(&sdata.data3max);
}

u8 key;

void debug(){
    key=KEY_Scan(0);
    if(key)
    {						   
        switch(key)
        {				 
            case 4:	//控制蜂鸣器
                BEEP=!BEEP;
                break;
            case 1:	//控制LED0翻转
                LED0=!LED0;
                getmax();
                break;
            case 2:	//控制LED1翻转	 
                LED1=!LED1;
                break;
            case 3:	//同时控制LED0,LED1翻转 
                LED0=!LED0;
                LED1=!LED1;
                getmin();
            
//                autoUpload();
//            printf("result:%d \r\n", get_shoushi(sdata.data0max - sdata.data0min, sdata.data1max - sdata.data1min, sdata.data2max - sdata.data2min, sdata.data3max - sdata.data3min));
            
                break;
        }
    }

}


