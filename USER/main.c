#include "sys.h"
#include "delay.h"
#include "usart.h"
#include "led.h"
#include "beep.h"
#include "key.h"
#include "lcd.h"
#include "fdc2214.h"
#include "bsp.h"
#include "filter.h"
#include "oled.h"
#include "solve.h"
#include "upload.h"
#include "debug.h"
#include "knnTest.h"
//#include "knndata.h"

#include "math.h"
#include "stdlib.h"


extern struct FDC fdcdata;                  //当前的测量值
extern struct FIL fdcfilter;                //滤波后的测量值
extern avefilterd data_filter_struct[4];   //结构体
extern float inDataAll[4];                 //传当前值，返回滤波后的值
extern struct SFLAG stableFlag;            //稳定标志
extern float lastmean[4][5];               //4个通道前5次的平均值，递推窗口
extern struct s sdata;

extern float train3[45][5] ;       //剪刀石头布训练数据
extern float train4[60][5] ;      //1 2 3 4 5 训练数据

int num = 0, submode = 1, downflag = 0, upflag = 0;
float stablevar;    // 平稳时的方差

void get_data(){
        get_fdcall();           //获取所有通道数据
        
        fdcdata.data0 /= 100;   
        fdcdata.data1 /= 100;
        fdcdata.data2 /= 100;
        fdcdata.data3 /= 100;
        
//        inDataAll[0] = fdcdata.data0;
//        inDataAll[1] = fdcdata.data1;
//        inDataAll[2] = fdcdata.data2;
//        inDataAll[3] = fdcdata.data3;
        
//        kalmanFilter_ALL();     //卡尔曼滤波
//        
//        fdcfilter.data0f = inDataAll[0];  //滤波后的额值
//        fdcfilter.data1f = inDataAll[1];
//        fdcfilter.data2f = inDataAll[2];
//        fdcfilter.data3f = inDataAll[3];
    
        delay_ms(1);
}


void oled_showdata(){       // oled显示每个通道的测量值
    char oledbuf[20] = {0};
    
    sprintf(oledbuf, "%d   ", fdcdata.data0);
    OLED_ShowString(40, 0, (u8 *)oledbuf);
    
    sprintf(oledbuf, "%d   ", fdcdata.data1);
    OLED_ShowString(40, 2, (u8 *)oledbuf);
    
    sprintf(oledbuf, "%d   ", fdcdata.data2);
    OLED_ShowString(40, 4, (u8 *)oledbuf);
    
    sprintf(oledbuf, "%d   ", fdcdata.data3);
    OLED_ShowString(40, 6, (u8 *)oledbuf);
}

void goto_1(){
    u8 key;
    int t = 0;
    
    OLED_ShowCHinese(63, 0, 7);     //识别模式
    OLED_ShowCHinese(79, 0, 8);
    OLED_ShowCHinese(95, 0, 9);
    OLED_ShowCHinese(111, 0, 10);
    
    while(1){
        key = KEY_Scan(0);
        if(key == 4){   //返回这题
            LED1=!LED1;
            return;
        }
        
        get_data();     //更新传感器测量值
        stablevar = get_stablevar();    //获取稳定状态
        
        // uploadF(0xF1, &lastmean[0][0], &lastmean[1][0], &lastmean[2][0], &lastmean[3][0]); //上传波形数据
        
        if(t % 2 == 0){ //会检测到两次跳边，第一次是手靠近时，第二次是手离开时， 取偶数次的跳变
            if(downflag == 0 && stablevar > 200){   // stablevar表示丽娜徐二十个测量值的方差， 大于200时表示有跳变
                downflag = 1;       //表示下降
            }
            
            if(downflag == 1 && stablevar < 200)    //方差小于200，表示波形平稳了
            {
                downflag = 0;
                print_shoushi(1);   //识别手势
                t++;
            }
        }else{
            //这是手离开后的跳变，应该忽略
            if(upflag == 0 && stablevar > 200){
                upflag = 1;       //表示上升
                OLED_ShowString(48, 4, "next");
            }
            
            if(upflag == 1 && stablevar < 200)
            {
                upflag = 0;
                t = 0;
            }
        }

    }
}

void goto_2(){
    int t = 0;
    u8 key;
    OLED_ShowCHinese(63, 0, 7);     //识别模式
    OLED_ShowCHinese(79, 0, 8);
    OLED_ShowCHinese(95, 0, 9);
    OLED_ShowCHinese(111, 0, 10);
    
    while(1){
       key = KEY_Scan(0);
        if(key == 4){
            LED1=!LED1;
            return;
        }
        
        key = KEY_Scan(0);
        if(key == 4){   //返回这题
            LED1=!LED1;
            return;
        }
        
        get_data();     //更新传感器测量值
        stablevar = get_stablevar();    //获取稳定状态
        
        // uploadF(0xF1, &lastmean[0][0], &lastmean[1][0], &lastmean[2][0], &lastmean[3][0]); //上传波形数据
        
        if(t % 2 == 0){ //会检测到两次跳边，第一次是手靠近时，第二次是手离开时， 取偶数次的跳变
            if(downflag == 0 && stablevar > 200){   // stablevar表示丽娜徐二十个测量值的方差， 大于200时表示有跳变
                downflag = 1;       //表示下降
            }
            
            if(downflag == 1 && stablevar < 200)    //方差小于200，表示波形平稳了
            {
                downflag = 0;
                print_shoushi(2);   //识别手势
                t++;
            }
        }else{
            //这是手离开后的跳变，应该忽略
            if(upflag == 0 && stablevar > 200){
                upflag = 1;       //表示上升
                OLED_ShowString(48, 4, "next");
            }
            
            if(upflag == 1 && stablevar < 200)
            {
                upflag = 0;
                t = 0;
            }
        }

        
    }
}

void get_train3(){ //石头剪刀布训练    录取顺序：石头 剪刀 布
    u8 key;
    u8 trainnum = 0;
//    u8 nums = 0;
//    u8 FinishFlag = 0;
    
    OLED_ShowCHinese(63, 0, 5);     //训练模式
    OLED_ShowCHinese(79, 0, 6);
    OLED_ShowCHinese(95, 0, 9);
    OLED_ShowCHinese(111, 0, 10);
    
    while(1){
        key = KEY_Scan(0);
        if(key == 1){
            LED0=!LED0;
            OLED_ShowString(24, 4, "              "); 
            OLED_ShowCHinese(40, 4, 15);     //初始化
            OLED_ShowCHinese(56, 4, 16);
            OLED_ShowCHinese(72, 4, 17);
            OLED_ShowString(88, 4, "  ");  
            
            delay_ms(500);
            
            OLED_ShowCHinese(32, 4, 0);     //石头第1次
            OLED_ShowCHinese(48, 4, 1);
            OLED_ShowCHinese(64, 4, 18);
            OLED_ShowChar(80, 4, '1');
            OLED_ShowCHinese(96, 4, 19);
            
            get_data();    //读取传感器值
            getmax();
        }else if(key == 3){
            LED0=!LED0;
            LED1=!LED1;
            get_data();    //读取传感器值
            getmin();
            trainnum++;
            printf("%d \r\n",trainnum);
            switch(trainnum-1)
            {
                case 0:
                    train3[trainnum + 35][0] = 0;
                    train3[trainnum + 35][1] = sdata.data0max - sdata.data0min;
                    train3[trainnum + 35][2] = sdata.data1max - sdata.data1min;
                    train3[trainnum + 35][3] = sdata.data2max - sdata.data2min;
                    train3[trainnum + 35][4] = sdata.data3max - sdata.data3min;
                    OLED_ShowCHinese(32, 4, 0);     //石头第2次
                    OLED_ShowCHinese(48, 4, 1);
                    OLED_ShowCHinese(64, 4, 18);
                    OLED_ShowChar(80, 4, '2');
                    OLED_ShowCHinese(96, 4, 19);
                    break;
                case 1:
                    train3[trainnum + 35][0] = 0;
                    train3[trainnum + 35][1] = sdata.data0max - sdata.data0min;
                    train3[trainnum + 35][2] = sdata.data1max - sdata.data1min;
                    train3[trainnum + 35][3] = sdata.data2max - sdata.data2min;
                    train3[trainnum + 35][4] = sdata.data3max - sdata.data3min;
                    OLED_ShowCHinese(32, 4, 0);     //石头第3次
                    OLED_ShowCHinese(48, 4, 1);
                    OLED_ShowCHinese(64, 4, 18);
                    OLED_ShowChar(80, 4, '3');
                    OLED_ShowCHinese(96, 4, 19);
                    break;
                case 2:
                    train3[trainnum + 35][0] = 0;
                    train3[trainnum + 35][1] = sdata.data0max - sdata.data0min;
                    train3[trainnum + 35][2] = sdata.data1max - sdata.data1min;
                    train3[trainnum + 35][3] = sdata.data2max - sdata.data2min;
                    train3[trainnum + 35][4] = sdata.data3max - sdata.data3min;
                    OLED_ShowCHinese(32, 4, 2);     //剪刀第1次
                    OLED_ShowCHinese(48, 4, 3);
                    OLED_ShowCHinese(64, 4, 18);
                    OLED_ShowChar(80, 4, '1');
                    OLED_ShowCHinese(96, 4, 19);
                    break;
                case 3:
                    train3[trainnum + 35][0] = 1;
                    train3[trainnum + 35][1] = sdata.data0max - sdata.data0min;
                    train3[trainnum + 35][2] = sdata.data1max - sdata.data1min;
                    train3[trainnum + 35][3] = sdata.data2max - sdata.data2min;
                    train3[trainnum + 35][4] = sdata.data3max - sdata.data3min;
                    OLED_ShowCHinese(32, 4, 2);     //剪刀第2次
                    OLED_ShowCHinese(48, 4, 3);
                    OLED_ShowCHinese(64, 4, 18);
                    OLED_ShowChar(80, 4, '2');
                    OLED_ShowCHinese(96, 4, 19);
                    break;
                case 4:
                    train3[trainnum + 35][0] = 1;
                    train3[trainnum + 35][1] = sdata.data0max - sdata.data0min;
                    train3[trainnum + 35][2] = sdata.data1max - sdata.data1min;
                    train3[trainnum + 35][3] = sdata.data2max - sdata.data2min;
                    train3[trainnum + 35][4] = sdata.data3max - sdata.data3min;
                    OLED_ShowCHinese(32, 4, 2);     //剪刀第3次
                    OLED_ShowCHinese(48, 4, 3);
                    OLED_ShowCHinese(64, 4, 18);
                    OLED_ShowChar(80, 4, '3');
                    OLED_ShowCHinese(96, 4, 19);
                    break;
                case 5:
                    train3[trainnum + 35][0] = 1;
                    train3[trainnum + 35][1] = sdata.data0max - sdata.data0min;
                    train3[trainnum + 35][2] = sdata.data1max - sdata.data1min;
                    train3[trainnum + 35][3] = sdata.data2max - sdata.data2min;
                    train3[trainnum + 35][4] = sdata.data3max - sdata.data3min;
                    OLED_ShowString(32, 4, "             ");
                    OLED_ShowCHinese(32, 4, 4);     //布第1次
                    OLED_ShowCHinese(48, 4, 18);
                    OLED_ShowChar(64, 4, '1');
                    OLED_ShowCHinese(80, 4, 19);
                    break;
                case 6:
                    train3[trainnum + 35][0] = 2;
                    train3[trainnum + 35][1] = sdata.data0max - sdata.data0min;
                    train3[trainnum + 35][2] = sdata.data1max - sdata.data1min;
                    train3[trainnum + 35][3] = sdata.data2max - sdata.data2min;
                    train3[trainnum + 35][4] = sdata.data3max - sdata.data3min;
                    OLED_ShowCHinese(32, 4, 4);     //布第2次
                    OLED_ShowCHinese(48, 4, 18);
                    OLED_ShowChar(64, 4, '2');
                    OLED_ShowCHinese(80, 4, 19);
                    break;
                case 7:
                    train3[trainnum + 35][0] = 2;
                    train3[trainnum + 35][1] = sdata.data0max - sdata.data0min;
                    train3[trainnum + 35][2] = sdata.data1max - sdata.data1min;
                    train3[trainnum + 35][3] = sdata.data2max - sdata.data2min;
                    train3[trainnum + 35][4] = sdata.data3max - sdata.data3min;
                    OLED_ShowCHinese(32, 4, 4);     //布第3次
                    OLED_ShowCHinese(48, 4, 18);
                    OLED_ShowChar(64, 4, '3');
                    OLED_ShowCHinese(80, 4, 19);
                    break;
                case 8:
                    train3[trainnum + 35][0] = 2;
                    train3[trainnum + 35][1] = sdata.data0max - sdata.data0min;
                    train3[trainnum + 35][2] = sdata.data1max - sdata.data1min;
                    train3[trainnum + 35][3] = sdata.data2max - sdata.data2min;
                    train3[trainnum + 35][4] = sdata.data3max - sdata.data3min;
                    OLED_ShowCHinese(32, 4, 5);     //训练结束
                    OLED_ShowCHinese(48, 4, 6);
                    OLED_ShowCHinese(64, 4, 20);
                    OLED_ShowCHinese(80, 4, 21);
                    break;
                default:
                    break;
            }
        }else if(key == 4){
            LED1=!LED1;
            return;
		}

		
    }
}

void goto_3(){
    int t = 0;
    u8 key;
    u8 trainnum = 0;

    get_train3();
    
    OLED_ShowCHinese(63, 0, 7);     //识别模式
    OLED_ShowCHinese(79, 0, 8);
    OLED_ShowCHinese(95, 0, 9);
    OLED_ShowCHinese(111, 0, 10);
    
    OLED_ShowString(24, 4, "               "); 
    
   while(1){
        key = KEY_Scan(0);
        if(key == 4){
            LED1=!LED1;
            return;
        }
        key = KEY_Scan(0);
        if(key == 4){   //返回这题
            LED1=!LED1;
            return;
        }
        
        get_data();     //更新传感器测量值
        stablevar = get_stablevar();    //获取稳定状态
        
        // uploadF(0xF1, &lastmean[0][0], &lastmean[1][0], &lastmean[2][0], &lastmean[3][0]); //上传波形数据
        
        if(t % 2 == 0){ //会检测到两次跳边，第一次是手靠近时，第二次是手离开时， 取偶数次的跳变
            if(downflag == 0 && stablevar > 200){   // stablevar表示丽娜徐二十个测量值的方差， 大于200时表示有跳变
                downflag = 1;       //表示下降
            }
            
            if(downflag == 1 && stablevar < 200)    //方差小于200，表示波形平稳了
            {
                downflag = 0;
                print_shoushi(3);   //识别手势
                t++;
            }
        }else{
            //这是手离开后的跳变，应该忽略
            if(upflag == 0 && stablevar > 200){
                upflag = 1;       //表示上升
                OLED_ShowString(48, 4, "next");
            }
            
            if(upflag == 1 && stablevar < 200)
            {
                upflag = 0;
                t = 0;
            }
        }

   }
}

void get_train4(){ //1 2 3 4 5 训练    录取顺序：1 2 3 4 5
    u8 key;
    u8 trainnum = -1;
    u8 nums = 0;
    u8 FinishFlag = 0;
    
    OLED_ShowCHinese(63, 0, 5);     //训练模式
    OLED_ShowCHinese(79, 0, 6);
    OLED_ShowCHinese(95, 0, 9);
    OLED_ShowCHinese(111, 0, 10);
    
    while(1){
        key = KEY_Scan(0);
        if(key == 1){
            LED0=!LED0;
            OLED_ShowString(24, 4, "              "); 
            OLED_ShowCHinese(40, 4, 15);     //初始化
            OLED_ShowCHinese(56, 4, 16);
            OLED_ShowCHinese(72, 4, 17);
            OLED_ShowString(88, 4, "  ");  
            
            delay_ms(500);
            
            OLED_ShowString(24, 4, "               "); 
            OLED_ShowChar(48, 4, '1');      //1 第一次
            OLED_ShowCHinese(64, 4, 18);
            OLED_ShowChar(80, 4, '1');
            OLED_ShowCHinese(96, 4, 19);

            get_data();    //读取传感器值
            getmax();
            
        }else if(key == 3){
            LED0=!LED0;
            LED1=!LED1;
            get_data();    //读取传感器值
            getmin();
            trainnum++;
            switch(trainnum)
            {
                case 0:
                    train4[trainnum + 45][0] = 0;
                    train4[trainnum + 45][1] = sdata.data0max - sdata.data0min;
                    train4[trainnum + 45][2] = sdata.data1max - sdata.data1min;
                    train4[trainnum + 45][3] = sdata.data2max - sdata.data2min;
                    train4[trainnum + 45][4] = sdata.data3max - sdata.data3min;
                    OLED_ShowChar(48, 4, '1');      //1 第二次
                    OLED_ShowCHinese(64, 4, 18);
                    OLED_ShowChar(80, 4, '2');
                    OLED_ShowCHinese(96, 4, 19);
                    break;
                case 1:
                    train4[trainnum + 45][0] = 0;
                    train4[trainnum + 45][1] = sdata.data0max - sdata.data0min;
                    train4[trainnum + 45][2] = sdata.data1max - sdata.data1min;
                    train4[trainnum + 45][3] = sdata.data2max - sdata.data2min;
                    train4[trainnum + 45][4] = sdata.data3max - sdata.data3min;
                    OLED_ShowChar(48, 4, '1');      //1 第三次
                    OLED_ShowCHinese(64, 4, 18);
                    OLED_ShowChar(80, 4, '3');
                    OLED_ShowCHinese(96, 4, 19);
                    break;
                case 2:
                    train4[trainnum + 45][0] = 0;
                    train4[trainnum + 45][1] = sdata.data0max - sdata.data0min;
                    train4[trainnum + 45][2] = sdata.data1max - sdata.data1min;
                    train4[trainnum + 45][3] = sdata.data2max - sdata.data2min;
                    train4[trainnum + 45][4] = sdata.data3max - sdata.data3min;
                    OLED_ShowChar(48, 4, '2');      //2 第一次
                    OLED_ShowCHinese(64, 4, 18);
                    OLED_ShowChar(80, 4, '1');
                    OLED_ShowCHinese(96, 4, 19);
                    break;
                case 3:
                    train4[trainnum + 45][0] = 1;
                    train4[trainnum + 45][1] = sdata.data0max - sdata.data0min;
                    train4[trainnum + 45][2] = sdata.data1max - sdata.data1min;
                    train4[trainnum + 45][3] = sdata.data2max - sdata.data2min;
                    train4[trainnum + 45][4] = sdata.data3max - sdata.data3min;
                    OLED_ShowChar(48, 4, '2');      //2 第二次
                    OLED_ShowCHinese(64, 4, 18);
                    OLED_ShowChar(80, 4, '2');
                    OLED_ShowCHinese(96, 4, 19);
                    break;
                case 4:
                    train4[trainnum + 45][0] = 1;
                    train4[trainnum + 45][1] = sdata.data0max - sdata.data0min;
                    train4[trainnum + 45][2] = sdata.data1max - sdata.data1min;
                    train4[trainnum + 45][3] = sdata.data2max - sdata.data2min;
                    train4[trainnum + 45][4] = sdata.data3max - sdata.data3min;
                    OLED_ShowChar(48, 4, '2');      //2 第三次
                    OLED_ShowCHinese(64, 4, 18);
                    OLED_ShowChar(80, 4, '3');
                    OLED_ShowCHinese(96, 4, 19);
                    break;
                case 5:
                    train4[trainnum + 45][0] = 1;
                    train4[trainnum + 45][1] = sdata.data0max - sdata.data0min;
                    train4[trainnum + 45][2] = sdata.data1max - sdata.data1min;
                    train4[trainnum + 45][3] = sdata.data2max - sdata.data2min;
                    train4[trainnum + 45][4] = sdata.data3max - sdata.data3min;
                    OLED_ShowChar(48, 4, '3');      //3 第一次
                    OLED_ShowCHinese(64, 4, 18);
                    OLED_ShowChar(80, 4, '1');
                    OLED_ShowCHinese(96, 4, 19);
                    break;
                case 6:
                    train4[trainnum + 45][0] = 2;
                    train4[trainnum + 45][1] = sdata.data0max - sdata.data0min;
                    train4[trainnum + 45][2] = sdata.data1max - sdata.data1min;
                    train4[trainnum + 45][3] = sdata.data2max - sdata.data2min;
                    train4[trainnum + 45][4] = sdata.data3max - sdata.data3min;
                    OLED_ShowChar(48, 4, '3');      //3 第二次
                    OLED_ShowCHinese(64, 4, 18);
                    OLED_ShowChar(80, 4, '2');
                    OLED_ShowCHinese(96, 4, 19);
                    break;
                case 7:
                    train4[trainnum + 45][0] = 2;
                    train4[trainnum + 45][1] = sdata.data0max - sdata.data0min;
                    train4[trainnum + 45][2] = sdata.data1max - sdata.data1min;
                    train4[trainnum + 45][3] = sdata.data2max - sdata.data2min;
                    train4[trainnum + 45][4] = sdata.data3max - sdata.data3min;
                    OLED_ShowChar(48, 4, '3');      //3 第三次
                    OLED_ShowCHinese(64, 4, 18);
                    OLED_ShowChar(80, 4, '3');
                    OLED_ShowCHinese(96, 4, 19);
                    break;
                case 8:
                    train4[trainnum + 45][0] = 2;
                    train4[trainnum + 45][1] = sdata.data0max - sdata.data0min;
                    train4[trainnum + 45][2] = sdata.data1max - sdata.data1min;
                    train4[trainnum + 45][3] = sdata.data2max - sdata.data2min;
                    train4[trainnum + 45][4] = sdata.data3max - sdata.data3min;
                    OLED_ShowChar(48, 4, '4');      //4 第一次
                    OLED_ShowCHinese(64, 4, 18);
                    OLED_ShowChar(80, 4, '1');
                    OLED_ShowCHinese(96, 4, 19);
                    break;
                case 9:
                    train4[trainnum + 45][0] = 3;
                    train4[trainnum + 45][1] = sdata.data0max - sdata.data0min;
                    train4[trainnum + 45][2] = sdata.data1max - sdata.data1min;
                    train4[trainnum + 45][3] = sdata.data2max - sdata.data2min;
                    train4[trainnum + 45][4] = sdata.data3max - sdata.data3min;
                    OLED_ShowChar(48, 4, '4');      //4 第二次
                    OLED_ShowCHinese(64, 4, 18);
                    OLED_ShowChar(80, 4, '2');
                    OLED_ShowCHinese(96, 4, 19);
                    break;
                case 10:
                    train4[trainnum + 45][0] = 3;
                    train4[trainnum + 45][1] = sdata.data0max - sdata.data0min;
                    train4[trainnum + 45][2] = sdata.data1max - sdata.data1min;
                    train4[trainnum + 45][3] = sdata.data2max - sdata.data2min;
                    train4[trainnum + 45][4] = sdata.data3max - sdata.data3min;
                    OLED_ShowChar(48, 4, '4');      //4 第三次
                    OLED_ShowCHinese(64, 4, 18);
                    OLED_ShowChar(80, 4, '3');
                    OLED_ShowCHinese(96, 4, 19);
                    break;
                case 11:
                    train4[trainnum + 45][0] = 3;
                    train4[trainnum + 45][1] = sdata.data0max - sdata.data0min;
                    train4[trainnum + 45][2] = sdata.data1max - sdata.data1min;
                    train4[trainnum + 45][3] = sdata.data2max - sdata.data2min;
                    train4[trainnum + 45][4] = sdata.data3max - sdata.data3min;
                    OLED_ShowChar(48, 4, '5');      //5 第一次
                    OLED_ShowCHinese(64, 4, 18);
                    OLED_ShowChar(80, 4, '1');
                    OLED_ShowCHinese(96, 4, 19);
                    break;
                case 12:
                    train4[trainnum + 45][0] = 4;
                    train4[trainnum + 45][1] = sdata.data0max - sdata.data0min;
                    train4[trainnum + 45][2] = sdata.data1max - sdata.data1min;
                    train4[trainnum + 45][3] = sdata.data2max - sdata.data2min;
                    train4[trainnum + 45][4] = sdata.data3max - sdata.data3min;
                    OLED_ShowChar(48, 4, '5');      //5 第二次
                    OLED_ShowCHinese(64, 4, 18);
                    OLED_ShowChar(80, 4, '2');
                    OLED_ShowCHinese(96, 4, 19);
                    break;
                case 13:
                    train4[trainnum + 45][0] = 4;
                    train4[trainnum + 45][1] = sdata.data0max - sdata.data0min;
                    train4[trainnum + 45][2] = sdata.data1max - sdata.data1min;
                    train4[trainnum + 45][3] = sdata.data2max - sdata.data2min;
                    train4[trainnum + 45][4] = sdata.data3max - sdata.data3min;
                    OLED_ShowChar(48, 4, '5');      //5 第三次
                    OLED_ShowCHinese(64, 4, 18);
                    OLED_ShowChar(80, 4, '3');
                    OLED_ShowCHinese(96, 4, 19);
                    break;
                case 14:
                    train4[trainnum + 45][0] = 4;
                    train4[trainnum + 45][1] = sdata.data0max - sdata.data0min;
                    train4[trainnum + 45][2] = sdata.data1max - sdata.data1min;
                    train4[trainnum + 45][3] = sdata.data2max - sdata.data2min;
                    train4[trainnum + 45][4] = sdata.data3max - sdata.data3min;
                    OLED_ShowCHinese(32, 4, 5);     //训练结束
                    OLED_ShowCHinese(48, 4, 6);
                    OLED_ShowCHinese(64, 4, 20);
                    OLED_ShowCHinese(80, 4, 21);
                    OLED_ShowString(96, 4, "  ");
                    break;
                default:
                    break;
            }
        }else if(key == 4){
            LED1=!LED1;
            return;
		}
        

        
		
    }
}

void goto_4(){
    
    u8 key;
    u8 trainnum = 0;
    
    get_train4();
    
    OLED_ShowCHinese(63, 0, 7);     //识别模式
    OLED_ShowCHinese(79, 0, 8);
    OLED_ShowCHinese(95, 0, 9);
    OLED_ShowCHinese(111, 0, 10);
    
    OLED_ShowString(24, 4, "               "); 
    
   while(1){
        key = KEY_Scan(0);
        if(key == 4){
            LED1=!LED1;
            return;
        }
        
        key = KEY_Scan(0);
        if(key == 4){   //返回这题
            LED1=!LED1;
            return;
        }
        
        get_data();     //更新传感器测量值
        stablevar = get_stablevar();    //获取稳定状态
        
        // uploadF(0xF1, &lastmean[0][0], &lastmean[1][0], &lastmean[2][0], &lastmean[3][0]); //上传波形数据
        
        if(t % 2 == 0){ //会检测到两次跳边，第一次是手靠近时，第二次是手离开时， 取偶数次的跳变
            if(downflag == 0 && stablevar > 200){   // stablevar表示丽娜徐二十个测量值的方差， 大于200时表示有跳变
                downflag = 1;       //表示下降
            }
            
            if(downflag == 1 && stablevar < 200)    //方差小于200，表示波形平稳了
            {
                downflag = 0;
                print_shoushi(4);   //识别手势
                t++;
            }
        }else{
            //这是手离开后的跳变，应该忽略
            if(upflag == 0 && stablevar > 200){
                upflag = 1;       //表示上升
                OLED_ShowString(48, 4, "next");
            }
            
            if(upflag == 1 && stablevar < 200)
            {
                upflag = 0;
                t = 0;
            }
        }

   }
}

void goto_x(int num){
    
    switch(num){
        case 1:
            goto_1();
            OLED_ShowString(40, 6, "1 over");
            delay_ms(1000);
            OLED_Clear();
            break;
        case 2:
            goto_2();
            OLED_ShowString(40, 6, "2 over");
            delay_ms(1000);
            OLED_Clear();
            break;
        case 3:
//            printf("bbbbbbbbbbbb\r\n");
            goto_3();
            OLED_ShowString(40, 6, "3 over");
            delay_ms(1000);
            OLED_Clear();
            break;
        case 4:
            goto_4();
            OLED_ShowString(40, 6, "4 over");
            delay_ms(1000);
            OLED_Clear();
            break;
    }
}

int main(void)
{ 
    u8 key;
    int t = 0;
    char oledbuf[20] = {0};
    int report = 1;
    float temp;
    
    int upflag = 0, staflag = 0, downflag = 0, countflag = 0;
    
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);//设置系统中断优先级分组2
	delay_init(168);        //初始化延时函数
	uart3_init(115200); 	//串口初始化 
    delay_ms(500);
LED_Init();				    //初始化LED端口  
	BEEP_Init();            //初始化蜂鸣器端口
    
    OLED_Init();			//初始化OLED  
    OLED_Clear(); 
    
    sprintf(oledbuf, "Num.%d ", num);
    OLED_ShowString(0, 0, (u8 *)oledbuf);

    
    
    /**********一起食用*******/
    IIC_Init();     //第一版本
//	InitSingleFDC2214_test();   // 单通道， 外部晶振
    InitMultiFDC2214();    // 多通道
    /**********一起食用*******/
    
//    ave_filter_init();  // 平均值窗口初始化
    
	LED0=0;					    //先点亮红灯
    
	while(1){
        
        key = KEY_Scan(0);
		if(key)
		{
			switch(key)
			{				 
				case 1:             //选择题号
                    LED0=!LED0;
					num++;
                    if( num > 4 ){
                        num = 0;
                    }
                    sprintf(oledbuf, "Num.%d ", num);
                    OLED_ShowString(0, 0, (u8 *)oledbuf);
					break;
				case 3:	            //确定进入功能
                    LED0=!LED0;
                    LED1=!LED1;
					goto_x(num);
					break;
                    
			}
            
            OLED_Clear();
            sprintf(oledbuf, "Num.%d ", num);
            OLED_ShowString(0, 0, (u8 *)oledbuf);
            
		}
            
                
//            uploadD(0xF1, &fdcdata.data0, &fdcdata.data1, &fdcdata.data2, &fdcdata.data3);    //上传波形数据， 配合匿名上位机使用
//            uploadF(0xF3, &inDataAll[0], &inDataAll[1], &inDataAll[2], &inDataAll[3]);
            

//            debug();  //录取数据
            
//        oled_showdata();
        
		delay_ms(1);
        
	}

}


